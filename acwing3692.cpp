// 输入两个字符串 s1,s2
// 。

// 输出最长连续公共子串长度和最长连续公共子串。

// 输入格式
// 一行，两个字符串 s1,s2
// ，用空格隔开。

// 输出格式
// 第一行输出最长连续公共子串的长度

// 第二行输出最长连续公共子串。如果不唯一，则输出 s1
//  中的最后一个。

// 数据范围
// 1≤|s1|,|s2|≤100

// 数据保证至少存在一个连续公共子串。

// 输入样例：
// abcdefg qwercdefiok
// 输出样例：
// 4
// cdef
//string的 find 返回的是下标位置
#include <bits/stdc++.h>
using namespace std;

int main()
{
    string s1, s2;
    cin >> s1 >> s2;
    int len1 = s1.size();
    int len2 = s2.size();
    int maxLen = 0; // 最长连续公共子串长度
    string result;  // 最长连续公共子串

    for (int i = 0; i < len1; ++i)
    {
        int p1 = i, p2 = 0;
        int len = 0;
        string tempResult;
        while (p1 < len1 && p2 < len2)
        {
            if (s1[p1] == s2[p2])
            {
                len++;
                tempResult += s1[p1];
                if (len > maxLen)
                {
                    maxLen = len;
                    result = tempResult;
                }
                p1++;
                p2++;
            }
            else
            {
                len = 0;
                tempResult.clear();
                p1 = i + 1;
                p2 = 0;
            }
        }
    }

    cout << maxLen << endl;
    cout << result << endl;

    return 0;
}
