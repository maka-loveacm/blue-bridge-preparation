#include<bits/stdc++.h>
using namespace std;
void GetNext(string p,int next[])
{
	int plen=p.length();
	next[0] = -1;
	int k=-1;
	int j=0;
	while(j < plen-1) //为什么-1 
	//速记：因为我们的next数组第一个值已经确定了，所以我们遍历next数组就需要少遍历一次
	{
		if(k==-1 || p[j]==p[k])
		{
			++k;
			++j;
			next[j]=k;//如果我们地位相同，我们共进退，并把你的位置记录给我的next数组下标中
		}
		else
		{
			k=next[k];//如果我们地位不匹配，那就之前的k滚回你自己记录的next数组的位置
		}
	}
}
int KmpSearch(string s, string p)
{
	int i = 0;
	int j = 0;
	int* next = (int*)malloc(p.length() * sizeof(int));//动态开辟一个一维数组next
	GetNext(p,next);
	int sLen = s.length();
	int pLen = p.length();
	while (i < sLen && j < pLen)
	{
		//①如果j = -1，或者当前字符匹配成功（即S[i] == P[j]），都令i++，j++
		if (j == -1 || s[i] == p[j])
		{
			i++;
			j++;
		}
		else
		{     
			j = next[j];
			//这个的本质也是回退，我们发现不管是求next数组，还是开始匹配的时候，代码回退的写法是一样的
		}
	}
	if (j == pLen)
		return i - j;
	else
		return -1;
}
// void getnext(string s,int next[])
//     {
//         int k=-1;
//         int j=0;
//         next[0]=-1;
// 		int plen=s.length();
//         while(j < plen-1) //已经初始化了一个 所以我们少赋值一个
//         {
//             if(k==-1 || s[k]==s[j])
//             {
//                 ++k;
//                 ++j;
//                 //赋值
//                 next[j]=k;
//             }
//             else
//             {
//                 k=next[k];//回退
//             }
//         }
//     }
//     int strStr(string haystack, string needle) 
//     {
//         int i=0;int j=0;
//         int* next=(int*)malloc(needle.length() * sizeof(int));
//         getnext(needle,next);
// 		int len1=haystack.length();
// 		int len2=needle.length();
//         while(i < len1 && j < len2)
//         {
//             if(j==-1 || haystack[i]==needle[j])
//             {
//                 i++;j++;//如果相等 就往前走
//             }
//             else
//             {
//                 j=next[j];//回退
//             }
//         }
//         if(j == needle.length())
//         {
//             return i-j;
//         }
//         else
//         {
//             return -1;
//         }
//     }
int main()
{
    ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	string s1="hello";
	string s2="ll";
	//int next[1001];
	// getnext(s2,next);
	// for(int i = 0;i<s2.length();++i)
	// {
	// 	cout << next[i] << " ";
	// }cout << endl;
	cout << strStr(s1,s2);
    return 0;
}