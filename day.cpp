#include <bits/stdc++.h>
using namespace std;
int main()
{
  int n=40;//cin >> n;
  int ans=0;
  for(int i=1;i<=n;++i)
  {
      string s=to_string(i);
      int flag=1;
      for(size_t j=0;j<s.size();j++)
      {
          if(flag && (s[j]=='1' || s[j]=='2' || s[j]=='9' || s[j]=='0'))
          {
              ans+=i;
              flag=0;
          }
      }
  }
  cout << ans << endl;
  return 0;
}