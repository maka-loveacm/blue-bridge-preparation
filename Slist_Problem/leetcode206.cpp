#include<bits/stdc++.h>
using namespace std;
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* reverseList(struct ListNode* head)
{
    //struct ListNode *newhead=NULL;
    struct ListNode *newhead=(struct ListNode *)malloc(sizeof(struct ListNode));
    newhead->next=NULL;
    newhead->val=0;
    struct ListNode * cur=head;
    while(cur)
    {
        struct ListNode * next=cur->next;//可以保证next一定存在，因为cur存在，那么next就一定存在，但是next不存在的话，next->next就一定不存在了！如果我们的next=next->next就会出现大问题~
        cur->next=newhead->next;
        newhead->next=cur;
        cur=next;
    }
    return newhead->next;
}
struct ListNode* reverseList1(struct ListNode* head) {
    struct ListNode* n1=NULL;
    struct ListNode* n2=head;
    while(n2)
    {
        struct ListNode* n3=n2->next;
        n2->next=n1;
        n1=n2;
        n2=n3;
    }
    return n1;
}

//彻底疯狂！！！！！！！！！！！！！！！！
int main()
{
    struct ListNode* n1=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n1);
    struct ListNode* n2=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n2);
    // struct ListNode* n3=(struct ListNode*)malloc(sizeof(struct ListNode));
    // assert(n3);
    n1->next=n2;
    n2->next=NULL;
    //n3->next=NULL;
    n1->val=1;
    n2->val=2;
    //n3->val=3;
    struct ListNode* newhead=reverseList(n1);
    while(newhead!=NULL)
    {
        cout << newhead->val << " " ;
        newhead=newhead->next;
    }
    return 0;
}

