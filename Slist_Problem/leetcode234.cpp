#include<bits/stdc++.h>
using namespace std;
struct ListNode {
    int val;
    struct ListNode *next;
};
struct ListNode* middleNode(struct ListNode* head) {
    struct ListNode* slow=head;
    struct ListNode* fast=head;
    while(fast && fast->next)
    {
        slow=slow->next;//slow走一步
        fast=fast->next->next;//fast走俩步
    }
    return slow;
}
struct ListNode* reverseList(struct ListNode* head)
{
    struct ListNode* n1=NULL;
    struct ListNode* n2=head;
    while(n2)
    {
        struct ListNode* n3=n2->next;
        n2->next=n1;
        
        //迭代
        n1=n2;
        n2=n3;
    }
    return n1;
}
//下面注释的代码，是我用来debug的，因为刚开始没有分析出来head链表到底还剩下什么元素
// bool isPalindrome(struct ListNode* head){
//     struct ListNode* mid=middleNode(head); //mid没有实际意义，就是中间节点，给下一个函数传参用的
//     struct ListNode* rmid=reverseList(mid);
//     while(mid!=NULL)
//     {
//         cout << mid->val << " " ;
//         mid=mid->next;
//     }
//     cout << endl;
//     while(rmid!=NULL)
//     {
//         cout << rmid->val << " " ;
//         rmid=rmid->next;
//     }
//     cout << endl;
//     while(head!=NULL)
//     {
//         cout << head->val << " " ;
//         head=head->next;
//     }
// }
bool isPalindrome(struct ListNode* head) 
{
    struct ListNode* rmid=(reverseList(middleNode(head)));//先找到链表的中间节点，再反转他
    struct ListNode* cur=head;
    while(cur && rmid)//开始遍历这俩个，都不可以为空，为空就结束
    {
        if(cur->val!=rmid->val)
        {
            return false;
        }
        cur=cur->next;
        rmid=rmid->next;
    }
    return true;
}
int main()
{
    struct ListNode* n1=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n1);
    struct ListNode* n2=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n2);
    struct ListNode* n3=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n3);
    struct ListNode* n4=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n4);
    struct ListNode* n5=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n5);
    n1->next=n2;
    n2->next=n3;
    n3->next=n4;
    n4->next=n5;
    n5->next=NULL;

    n1->val=1;
    n2->val=2;
    n3->val=3;
    n4->val=2;
    n5->val=1;

    cout << isPalindrome(n1) << endl;
    

 
    return 0;
}