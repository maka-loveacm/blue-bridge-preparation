/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* removeElements(struct ListNode* head, int val) {
    if(head==NULL)
    {
        return NULL;
    }
    struct ListNode* cur =head;
    struct ListNode* prev=NULL;//不用担心prev的节点更新问题
    //因为要尾插，不想用tail了，定义一个Guard
    struct ListNode* newhead=(struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* tail=newhead;
    while(cur)
    {
        if(cur->val!=val)
        {
            tail->next=cur;//更新尾节点链接
            //newhead->next=cur; 头节点别动！！！因为这个题不像反转链表，反转链表的newhead往前更新，因为箭头的指向在改变，所以头的改变到时候到了末尾正好是正确的
            tail=cur;//更新尾节点位置
            cur=cur->next;
        }
        else
        {
            cur=cur->next;
            // struct ListNode* next = cur->next;
            // free(cur);
            // cur = next;
            //杭哥这样删除是为了防止内存泄漏，但我直接往下一步走了
        }
    }
    tail->next=NULL;//这句是一个大bug
    head=newhead->next;
    free(newhead);
    return head;
}