#include<bits/stdc++.h>
using namespace std;
struct ListNode {
    int val;
    struct ListNode *next;
};

 //快慢指针

struct ListNode* middleNode(struct ListNode* head) {
    struct ListNode* slow=head;
    struct ListNode* fast=head;
    while(fast && fast->next)
    {
        slow=slow->next;//slow走一步
        fast=fast->next->next;//fast走俩步
    }
    return slow;
}

int main()
{
    struct ListNode* n1=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n1);
    struct ListNode* n2=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n2);
    struct ListNode* n3=(struct ListNode*)malloc(sizeof(struct ListNode));
    assert(n3);
    n1->next=n2;
    n2->next=n3;
    n3->next=NULL;
    n1->val=1;
    n2->val=2;
    n3->val=3;
    struct ListNode* newhead=middleNode(n1);
    while(newhead!=NULL)
    {
        cout << newhead->val << " " ;
        newhead=newhead->next;
    }
    return 0;
}
