/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* partition(struct ListNode* head, int x) {
    struct ListNode* LessGuard=(struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* GreaterGuard=(struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* cur=head,*LessTail=LessGuard,*GreaterTail=GreaterGuard;
    //LessGuard->next=NULL;
    GreaterGuard->next = NULL;
    //这俩个赋予空值看起来没有意义，但其实用处很大，因为我们下面有一个赋值操作
    if(head==NULL)
    {
        return NULL;
    }
    if(head->next==NULL)
    {
        return head;
    }
    while(cur)
    {
        //struct ListNode* next=cur->next;
        //因为我们这个题并没有修改cur的值，所以不需要next变量让我们回去
        if(cur->val < x)
        {
            LessTail->next=cur;
            LessTail = cur;
            cur=cur->next;
        }
        else
        {
            GreaterTail->next=cur;
            GreaterTail=cur;
            cur=cur->next;
        }
    }
    LessTail->next=GreaterGuard->next;//将俩个链表连接起来
    GreaterTail->next=NULL;//避免藕断丝连
    
    //释放哨兵位头结点
    //head = LessGuard->next;
    // free(GreaterGuard);
    // free(LessGuard);
    // GreaterGuard = NULL;
    // LessGuard = NULL;
    return LessGuard->next;
}