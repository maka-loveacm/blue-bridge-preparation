/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
//返回倒数第K个节点
int kthToLast(struct ListNode* head, int k){
    struct ListNode* slow=head;
    struct ListNode* fast=head;
    if(head->next==NULL)
    {
        return head->val;
    }
    while(k--)
    {
        fast=fast->next;
    }
    while(fast)
    {
        slow=slow->next;
        fast=fast->next;
    }
    return slow->val;
}
