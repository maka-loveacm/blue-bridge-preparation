/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    //取小的尾插
    struct ListNode* guard=(struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* l1=list1,*l2=list2;
    //guard->next=NULL;
    struct ListNode* tail=guard;
    while(l1 && l2)
    {
        if(l1->val <= l2->val)
        {
            struct ListNode* next=l1->next;//方便l1可以回去原来的链表继续遍历
            tail->next=l1;//不断更新，只要最小的
            tail=l1;//tail向前移动一下
            l1=next;//l1继续回去遍历原先的链表
        }
        else
        {
            struct ListNode* next=l2->next;//方便l2可以回去原来的链表继续遍历
            tail->next=l2;//不断更新，只要最小的
            tail=l2;//tail向前移动一下
            l2=next;//l2继续回去遍历原先的链表
        }
    }
    if(l1==NULL)
    {
        tail->next=l2;
    }
    else
    {
        tail->next=l1;
    }
    //最后将guard释放，更规范
    struct ListNode* head = guard->next;
    free(guard);
    guard = NULL;
    return head;
}