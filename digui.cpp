#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=1e5+9;
ll dp[N];
ll fib(int n)
{
    if(dp[n])
    {
        return dp[n];
    }
    if(n<=2)
    {
        return 1;
    }
    else
    {
    	cout << "dp[n]:" <<  fib(n-1)+fib(n-2) << endl;
        return dp[n]=(fib(n-1)+fib(n-2));
    }
}
int main()
{
    int n=10;
    //cin >> n;
    for(int i=1;i<=n;++i)
    {
        cout << fib(i) << endl;
    }
    return 0;
}