#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll x=0;
int test_every_to_10(int m, string s)
{
    int ans = 0;
    int len = s.size();
    for (int i = len - 1; i >= 0; --i)
    {
        char t = s[i];//s[len]是不存在的 所以我们的初始值是 len-1
        if (t >= '0' && t <= '9')
        {
            ans += (t - '0') * pow(m, len - 1 - i);
        }
        else
        {
            ans += (t - 'a' + 10) * pow(m, len - 1 - i);
        }
    }
    return ans;
}    
//n是待转换的十进制数，m是待转换成的进制数 
string test_10_to_every(int n,int m)
{
    string ans="";
    do
    {
        int t=n%m;
        if(t>=0&&t<=9)    
			ans+=(t+'0');//尾插
        else 
			ans+=(t+'a'-10);
        n/=m;
    }while(n);
    reverse(ans.begin(),ans.end());
    return ans;
}
int main()
{
    string a="1101";
    cout << test_every_to_10(2,a) << endl;
    cout << test_10_to_every(13,2) << endl;
    return 0;
}