#include<bits/stdc++.h>
using namespace std;

int lengthOfLIS(int* nums, int numsSize) {
    int dp[numsSize];
    for (int i = 0; i < numsSize; i++) {
        dp[i] = 1; // 初始状态，每个数字自身构成长度为 1 的子序列
        for (int j = 0; j < i; j++) {
            if (nums[j] < nums[i]) {
                dp[i] = fmax(dp[i], dp[j] + 1); // 更新 dp[i]
            }
        }
    }
    
    int ans = 0;
    for (int i = 0; i < numsSize; i++) {
        ans = fmax(ans, dp[i]); // 寻找 dp 数组中最大的值
    }
    
    return ans;
}

int main()
{
    return 0;
}