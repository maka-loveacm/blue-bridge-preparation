#include<bits/stdc++.h>
using namespace std;
int dp[1001],w[1001],v[1001];
int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int num,n,V,i,j;
    cin >> num;
    while(num--)
    {
        cin >> n >> V;
        for(int i=0;i<n;++i) cin >> w[i];
        for(int i=0;i<n;++i) cin >> v[i];
        for(int i=0;i<n;i++)
        {
            for(j=V;V>=v[i];j--)
            {
                dp[j]=max(dp[j],dp[j-v[i]]+w[i]);
            }
        }
        cout << dp[V];
    }
    return 0;
}