#include<bits/stdc++.h>
using namespace std;
int findLengthOfLCIS(int* nums, int numsSize) {
    if(numsSize==1)
    {
        return 1;
    }
    int i=1;int j=0;
    int dp[numsSize];
    dp[0]=1;
    for(;i<numsSize;++i)
    {
        dp[i]=1;
        if(nums[i] > nums[j])
        {
            dp[i]=max(dp[i],dp[j]+1);
            j++;
        }
        else
        {
            j=i;
        }
    }
    int ans=0;
    for(int i=0;i<numsSize;++i)
    {
        ans = max(ans,dp[i]);
    }
    return ans;
}
int findLengthOfLCIS_carl(vector<int>& nums) {
        if (nums.size() == 0) return 0;
        int result = 1;
        vector<int> dp(nums.size() ,1);
        for (int i = 1; i < nums.size(); i++) {
            if (nums[i] > nums[i - 1]) { // ������¼
                dp[i] = dp[i - 1] + 1;
            }
            if (dp[i] > result) result = dp[i];
        }
        return result;
}
int main()
{
    int nums[]={1,3,5,4,7};
    cout << findLengthOfLCIS(nums,5);
    return 0;
}