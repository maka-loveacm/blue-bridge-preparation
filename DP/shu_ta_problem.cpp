#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

const int N = 1010;//多开一点，那么默认的就都是0了
int a[N][N]={0};//存储数据
int n;//递归函数中需要设置边界,故将n设为全局变量
int f[N][N]={0};//相当于备忘录 记录最长路径，已经是加上节点的那种
int dfs(int x,int y)//x表示行,y表示列
{
    if (x == n) return a[x][y];//递归边界
    return max(dfs(x + 1,y),dfs(x + 1,y + 1)) + a[x][y];
}

int main()
{
    cin >> n;
    
    for (int i = 1;i <= n;i++)//读入数塔:数塔有n行
        for (int j = 1;j <= i;j++)//每行有n列
            cin >> a[i][j];
    
    //cout << dfs(1,1) << endl;//dfs(1,1):从第1行第1列到边界的最大路径
    for(int i=n;i>=1;i--)
    {
        for(int j=1;i<=j;j++)
        {
            //因为多开辟了一些，所以不存在数组越界
            f[i][j]=max(f[i+1][j],f[i+1][j+1])+a[i][j];
        }
    }
    cout << f[1][1] << endl;
    return 0;
}
