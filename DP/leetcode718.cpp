#include<bits/stdc++.h>
using namespace std;
class Solution {
public:
    int findLength(vector<int>& nums1, vector<int>& nums2) 
	{
    	vector<vector<int>> dp (nums1.size() + 1, vector<int>(nums2.size() + 1, 0));
    	int result=0;
        //代码循环细节，正常来说我们的size就是元素个数，不是数组边界的大小，那么我们i初始化成1 循环终止条件为 i<=size 这样是会发生越界的 但是由于我们的dp数组定义的是 i-1 j-1 为下标的数组，所以我们必须要到达边界，这样才可以访问到我们的最后一个元素 无论是最长递增 还是最长公共 都是 i<nums1Size 这样子的 说明细节真的很多
    	for(int i=1;i<=nums1.size();i++)
    	{
    	    for(int j=1;j<=nums2.size();j++)
        	{
            	if(nums1[i-1]==nums2[j-1])//这个if条件就忘了写了 光顾着写递推公式 连最基本的条件判断都没写
            	{
                	dp[i][j]=dp[i-1][j-1]+1;
            	}
            	if(dp[i][j] > result) result =dp[i][j];
                //不需要再遍历一次二维数组，我们通过result顺便保存
        	}
    	}
        for(size_t i=0;i<=nums1.size();i++)
        {
            for(size_t j=0;j<=nums2.size();j++)
            {
                cout << dp[i][j] << " ";
            }cout << endl;
        }
    	return result;
    }
};
int main()
{
    ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	Solution s;
    vector<int>nums1={1,2,3,2,1};
    vector<int>nums2={3,2,1,4,7};
    s.findLength(nums2,nums1);
    return 0;
}