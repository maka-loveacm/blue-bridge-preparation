#include <bits/stdc++.h>
using namespace std;

struct pos {
    int level;  // 当前楼层
    int steps;  // 步数
};

int N, start, End;
int jump[202];  // 存放楼层数字
int vis[202];  // 检查是否存放过

void bfs() {
    pos cur, next;
    //初始化当前节点为第一个start楼层
    cur.level = start;
    cur.steps = 0;

    //将已经初始化好的起点放在队列当中
    queue<pos> q;
    q.push(cur);
    vis[start] = 1;

    //循环判断队列是否为空，不空则说明有元素，那么取出删除
    while (!q.empty()) {
        cur = q.front();
        q.pop();

        //队列的首元素楼层就是我们的目标楼层 那么循环结束 输出我们走了多少步就好了
        if (cur.level == End) 
        {
            cout << cur.steps << endl;
            return;
        }

        next.steps = cur.steps + 1;///不管往上走还是往下走 步数都会加一

        // 向上跳
        next.level = cur.level + jump[cur.level];//下一个楼层=当前楼层+跳跃数
        //这个是最高楼层的特判
        if (next.level >= 1 && next.level <= N && vis[next.level] == 0) {
            vis[next.level] = 1;//没有来过，做标记，继续做延申，目的是为了剪枝
            q.push(next);
        }

        // 向下跳
        next.level = cur.level - jump[cur.level];
        if (next.level >= 1 && next.level <= N && vis[next.level] == 0) {
            vis[next.level] = 1;
            q.push(next);
        }
    }

    cout << "-1" << endl;
}

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    while (cin >> N) {
        if (N == 0) break;

        cin >> start >> End;

        for (int i = 1; i <= N; ++i) {
            cin >> jump[i];
            vis[i] = 0;
        }

        bfs();
    }

    return 0;
}
