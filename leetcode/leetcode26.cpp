#include<bits/stdc++.h>
using namespace std;
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int src=0,dst=0;
        while(src < nums.size())
        {
            if(nums[src]==nums[dst])
            {
                ++src;
            }
            else
            {
                nums[++dst]=nums[src];
                //这里优化一下 可以加上 ++src 因为赋值之后肯定相等了就
            }
        }
        return ++dst;
    }
};
int main()
{
    
    return 0;
}